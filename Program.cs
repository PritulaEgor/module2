﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return companyRevenue * companiesNumber * tax / 100;
        }

        public string GetCongratulation(int input)
        {
            if (input % 2 == 0 && input >= 18)
            {

                return "Поздравляю с совершеннолетием!";
            }
            else if (input % 2 == 1 && input > 12 && input < 18)
            {
                return "Поздравляю с переходом в старшую школу!";
            }
            else
            {
                return $"Поздравляю с {input}-летием!";
            }
        }

        public double GetMultipliedNumbers(string first, string second)
        {

            if (first.Contains(",") || second.Contains(","))
            {
                first = first.Replace(',', '.');
                second = second.Replace(',', '.');
            }

            if (double.TryParse(first, out double i) && double.TryParse(second, out double j))
            {
                return Convert.ToDouble(first) * Convert.ToDouble(second);
            }
            else
            {
                return 0;
            }
        }

        public double GetFigureValues(Figure figureType, Parameter parameterToCompute, Dimensions dimensions)
        {
            if (figureType == Figure.Circle)
            {
                if (parameterToCompute == Parameter.Perimeter)
                {

                    return Math.Round(2 * Math.PI * dimensions.Radius);
                }
                else
                {

                    return Math.Round(Math.Pow(dimensions.Radius, 2) * Math.PI);
                }
            }
            else if (figureType == Figure.Rectangle)
            {
                if (parameterToCompute == Parameter.Perimeter)
                {
                    return Math.Round(2 * dimensions.FirstSide + 2 * dimensions.SecondSide);
                }
                else
                {
                    return Math.Round(dimensions.FirstSide * dimensions.SecondSide);
                }
            }
            else
            {
                if (parameterToCompute == Parameter.Perimeter)
                {
                    return Math.Round(dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide);
                }
                else
                {
                    if (dimensions.Height != 0)
                    {
                        return Math.Round(dimensions.FirstSide * dimensions.Height / 2);
                    }
                    else
                    {
                        double halfPerimeter = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2;
                        return Math.Floor(Math.Sqrt(halfPerimeter * (halfPerimeter - dimensions.FirstSide) * (halfPerimeter - dimensions.SecondSide) * (halfPerimeter - dimensions.ThirdSide)));
                    }
                }
            }
        }
    }
}
